'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect');

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        fonts: 'build/fonts/',
        jade: 'build/',
        sass: 'build/css'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/*.js',
        sass: 'src/sass/**/*.sass',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        jade: 'src/jade/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        fonts: 'src/fonts/**/*.*',
        jade: 'src/jade/**/*.*',
        sass: 'src/sass/**/*.*'

    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Devil"
};


gulp.task('jade', function() {
    gulp.src(path.src.jade)
        .pipe(jade())
        .pipe(gulp.dest(path.build.jade))
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('connect', function () {
    connect.server({
        root: 'build',
        livereload: true
    });
});//my code


gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('sass:build', function () {
    gulp.src(path.src.sass)
        .pipe(sourcemaps.init())
        .pipe(sass({
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.sass))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        // .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});



gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}));
});


gulp.task('jade:build', function() {
    gulp.src(path.src.jade)
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest(path.build.jade))
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'js:build',
    'fonts:build',
    'jade:build',
    'sass:build'

]);


gulp.task('watch', function(){

    watch([path.watch.jade], function(event, cb) {
        gulp.start('jade:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });

    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');

    });

    watch([path.watch.sass], function(event, cb) {
        gulp.start('sass:build');

    });

});


gulp.task('default', ['build', 'webserver','connect', 'watch']);
